<?php

namespace HRis\Auth\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        
        $supportedLocales = collect(config('hris-saas.supported_locales'));

        if (($locale = $request->get('locale')) && $supportedLocales->contains($locale)) {
            $user->update(['last_saved_locale' => $locale]);
        }

        App::setLocale($user->fresh()->last_saved_locale);

        return $next($request);
    }
}
