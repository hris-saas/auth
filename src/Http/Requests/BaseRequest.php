<?php

namespace HRis\Auth\Http\Requests;

use Exception;
use HRis\Core\Http\Requests\BaseRequest as FormRequest;

class BaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        if ($user = $this->user()) {
            try {
                return $user->hasRole('super-admin') || $user->hasPermissionTo($this->route()->getName(), 'api');
            } catch (Exception $e) {
                return false;
            }
        }

        return true;
    }
}
