<?php

namespace HRis\Auth\Eloquent\Passport;

class PersonalAccessClient extends \Laravel\Passport\PersonalAccessClient
{
    public function getConnectionName()
    {
        return $this->connection;
    }
}
