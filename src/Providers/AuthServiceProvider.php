<?php

namespace HRis\Auth\Providers;

use Laravel\Passport\Passport;
use HRis\Auth\Eloquent\Passport\Token;
use HRis\Auth\Eloquent\Passport\Client;
use HRis\Auth\Eloquent\Passport\AuthCode;
use HRis\Core\Providers\BaseServiceProvider;
use HRis\Auth\Eloquent\Passport\PersonalAccessClient;

class AuthServiceProvider extends BaseServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->registerMigrations();
        }

        $this->registerTranslations();

        $this->loadViewsFrom(__DIR__.'/../../assets/views', 'auth');

        $this->registerConfigs();
    }

    /**
     * Register Auth's translation files.
     *
     * @return void
     */
    protected function registerTranslations(): void
    {
        $this->loadTranslationsFrom(__DIR__.'/../../assets/lang', 'auth');

        $this->publishes([
            __DIR__.'/../../assets/lang' => resource_path('lang/vendor/hris-saas/auth'),
        ], 'hris-saas::auth-translations');
    }

    /**
     * Register Auth's migration files.
     *
     * @return void
     */
    protected function registerMigrations(): void
    {
        $this->loadMigrationsFrom(__DIR__.'/../../assets/database/migrations');

        $this->publishes([
            __DIR__.'/../../assets/database/migrations' => database_path('migrations'),
        ], 'hris-saas::auth-migrations');
    }

    /**
     * Register Auth's config files.
     *
     * @return void
     */
    protected function registerConfigs(): void
    {
        // config for spatie permission
        $path = './vendor/spatie/laravel-permission/config/permission.php';

        if (realpath($newPath = app_path('../vendor/spatie/laravel-permission/config/permission.php'))) {
            $path = $newPath;
        }

        $this->mergeConfigFrom($path, 'permission');

        //
        $path = realpath(__DIR__.'/../../assets/config/config.php');

        $this->mergeConfigFrom($path, 'hris-saas');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register(): void
    {
        Passport::ignoreMigrations();

        Passport::useTokenModel(Token::class);
        Passport::useClientModel(Client::class);
        Passport::useAuthCodeModel(AuthCode::class);
        Passport::usePersonalAccessClientModel(PersonalAccessClient::class);
    }
}
