<?php

return [
    'database' => [

        'migrations' => [

            'order' => ['core', 'auth'],
        ],
    ],

    'permissions' => [

        'auth::logout',
        'auth::password.confirm',
    ],
];
