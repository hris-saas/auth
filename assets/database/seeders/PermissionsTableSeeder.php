<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissionsInConfig = config('hris-saas.permissions');

        $permissions = [];
        foreach ($permissionsInConfig as $permission) {
            $permissions[] = ['name' => $permission, 'guard_name' => 'api'];
        }

        DB::table('permissions')->delete();

        DB::table('permissions')->insert($permissions);
    }
}
